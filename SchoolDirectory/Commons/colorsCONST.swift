//
//  colorsCONST.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/16/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import Foundation
import UIKit

let BACKGROUND_COLOR = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1) // Background color for all the screen

let NAVIGATIONBAR_COLOR = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1) // Navigation Bar Color
let NAVIGATIONBAR_TITLE_COLOR = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1) // Title color of the navigation bar

let SCHOOLLIST_CELL_COLOR = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1) // Background color for the cell of school list

let SCHOOLDETAIL_SCHOOLTITLE_CELL_COLOR = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1) // School Title Cell Color for Detail Screen

let SCHOOLDETAIL_INFO_CELL_COLOR_1 = #colorLiteral(red: 0.07581086072, green: 0.2134651492, blue: 0.3089070491, alpha: 1) // School info cell color for detail screen
let SCHOOLDETAIL_INFO_CELL_COLOR_2 = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1) // School info cell color for gradient

let LOADERVIEW_COLOR_1 = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
let LOADERVIEW_COLOR_2 = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
