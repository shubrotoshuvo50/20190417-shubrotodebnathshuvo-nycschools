//
//  SchoolDetailViewController.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/16/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    @IBOutlet weak var schoolDetailTableView: UITableView! // Base Table View
    
    fileprivate let schoolMapCellId: String = "SchoolMapCellId" // Identifier for School cell map id
    fileprivate let schoolTitleCellId: String = "schoolTitleCellId" // Indentifier for school title Cell id
    fileprivate let schoolDetailDetailSectionCellId: String = "SchoolDetailDetailSectionCellId" // Indentifier for School Detail section cell id
    
    private var cellToDisplay: [ () -> UITableViewCell ] = [] // Cells to show in the table view
    private let queue = OperationQueue()
    
    private var tableViewParam: UITableView! // This will be set by the protocol func for reuse
    private var indexPathParam: IndexPath! // This will be set by the protocol func for reuse
    
    var schoolModel: SchoolModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI() // Do necessary UI Setup
        registerCells() // Register All Necessary Cells
        setupCellsToDisplay() // Setup cells array to display cells in order
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        queue.cancelAllOperations() // Cancel operation if user goes back before downloading data
    }

}

// All private function will go here
extension SchoolDetailViewController {
    // Setup UI and other fixes
    private func setupUI() {
        view.backgroundColor = BACKGROUND_COLOR // Common Background Color
        self.title = "Detail" // Title of the page
        
        // Automatic Cell Height Configuration
        schoolDetailTableView.rowHeight = UITableView.automaticDimension
        schoolDetailTableView.estimatedRowHeight = 20
    }
    
    // Register Cells for use
    private func registerCells() {
        schoolDetailTableView.register(UINib(nibName: "SchoolMapTableViewCell", bundle: nil), forCellReuseIdentifier: schoolMapCellId) // Register School Map Cell
        schoolDetailTableView.register(UINib(nibName: "SchoolDetailTitleTableViewCell", bundle: nil), forCellReuseIdentifier: schoolTitleCellId) // Register School Title Cell
        schoolDetailTableView.register(UINib(nibName: "SchoolDetailDetailSectionTableViewCell", bundle: nil), forCellReuseIdentifier: schoolDetailDetailSectionCellId) // Register School Detail Section Cell
    }
    
    // Setup cellToDisplay array which will be used to display cells
    private func setupCellsToDisplay() {
        cellToDisplay = [
            { return self.setupSchoolMapCell() },
            { return self.setupSchoolTitleCell() },
            { return self.setupSchoolDetailSectionCell() }
        ]
    }
}

// All custom function for specific cell goes here
extension SchoolDetailViewController {
    // Display School Map in this cell
    func setupSchoolMapCell() -> UITableViewCell {
        let cell = tableViewParam.dequeueReusableCell(withIdentifier: schoolMapCellId, for: indexPathParam) as! SchoolMapTableViewCell // Get the cell
        cell.setMapLocation(schoolName: schoolModel?.school_name ?? "Unknown School", latitude: schoolModel?.latitude, longitude: schoolModel?.longitude) // Set map location of the school
        return cell
    }
    
    // Display School Title in this cell
    func setupSchoolTitleCell() -> UITableViewCell {
        let cell = tableViewParam.dequeueReusableCell(withIdentifier: schoolTitleCellId, for: indexPathParam) as! SchoolDetailTitleTableViewCell
        cell.titleLabel.text = schoolModel?.school_name
        return cell
    }
    
    // Display the school details in this cell
    func setupSchoolDetailSectionCell() -> UITableViewCell {
        let cell = tableViewParam.dequeueReusableCell(withIdentifier: schoolDetailDetailSectionCellId, for: indexPathParam) as! SchoolDetailDetailSectionTableViewCell
        cell.layoutSubviews()
        queue.addOperation { // Add Operation in the queue
            cell.callServer(schoolDbn: self.schoolModel?.dbn ?? "")
        }
        queue.waitUntilAllOperationsAreFinished()
        return cell
    }
}

// Table View Protocols to display data
extension SchoolDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableViewParam = tableView // Set the Table View to tableViewParam for reuse
        indexPathParam = indexPath // Set the Index Path to IndexPathParam for reuse
        let cell = cellToDisplay[indexPath.row]() // Run the closure to get cell config for that cell
        return cell
    }
    
    
}
