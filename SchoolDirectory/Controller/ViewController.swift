//
//  ViewController.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/16/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import UIKit
import Network

class ViewController: UIViewController {

    @IBOutlet weak var schoolListCollectionView: UICollectionView!
    @IBOutlet weak var loadingView: UIView!
    fileprivate let schoolCellIdentifier: String = "SchoolCellId"
    fileprivate let schoolDetailStoryboardId: String = "SchooldetailStoryboardId"
    
    private let schoolListViewModel = SchoolListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell() // Register cell for use
        setupUI() // Setup all the UI of the view
        doServerCalls() // Get data from API
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCellSize() // Setup size of the cell
    }
}

// All Private Functions will go here
extension ViewController {
    // Register Cell for use
    private func registerCell() {
        schoolListCollectionView.register(UINib(nibName: "SchoolCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: schoolCellIdentifier)
    }
    
    // Setup UI color and other fixes
    private func setupUI() {
        view.backgroundColor = BACKGROUND_COLOR // Set Background color
        navigationController?.navigationBar.barTintColor = NAVIGATIONBAR_COLOR // Set color for the navigation
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: NAVIGATIONBAR_TITLE_COLOR] // Set Title Color for the navigation
        
        // Navigation Back Button Customization
        let backButton = UIBarButtonItem() // Intialize a nav button
        backButton.title = "" // Set title to nothing
        backButton.tintColor = NAVIGATIONBAR_TITLE_COLOR // Set color to title color
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton // Set the button to backbarbutton item
        
        setupLoaderView() // Create the loader view with animation and gradient
    }
    
    // Creates the loader view with animation and gradient
    private func setupLoaderView() {
        let gradient = CAGradientLayer(layer: loadingView.layer) // Create gradient layer
        gradient.colors = [LOADERVIEW_COLOR_1.cgColor, LOADERVIEW_COLOR_2.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        gradient.frame = loadingView.bounds
        gradient.locations = [0,0.4]
        loadingView.layer.insertSublayer(gradient, at: 0)
        
        let animation = CABasicAnimation(keyPath: "locations") // Intialize animation key
        animation.fromValue = [0,0.4]
        animation.toValue = [0,1.7]
        animation.repeatCount = Float.infinity
        animation.autoreverses = true // Set reverse to true
        animation.duration = 0.5
        gradient.add(animation, forKey: nil)
    }
    
    // Setup the size of the cell
    private func setupCellSize() {
        let layout = schoolListCollectionView.collectionViewLayout as! UICollectionViewFlowLayout // Setup Layout
        
        let itemInOneLine: CGFloat = 1 // Total Item in one line
        let itemSpacing: CGFloat = 20 // Space in the sides of the cell
        let itemLineSpacing: CGFloat = 20 // Space between the cells
        let sideInset: CGFloat = 10 // Space beside the side of the collection View
        
        layout.sectionInset = UIEdgeInsets(top: 20, left: sideInset, bottom: 20, right: sideInset) // Set the inset
        
        let rowWidth = schoolListCollectionView.frame.width - itemSpacing * CGFloat(itemInOneLine - 1) - (sideInset * 2) // Determine the width of the row eliminating all the spaces
        let cellWidth = rowWidth/itemInOneLine // Determine the Width of a particular cell
        
        layout.itemSize = CGSize(width: cellWidth, height: 80) // Set the size of the cell
        layout.minimumLineSpacing = itemLineSpacing // Set the line spacing
        layout.minimumInteritemSpacing = itemSpacing // Set the spacing between items
    }
    
    // Server Call to get School List
    private func doServerCalls() {
        let monitor = NWPathMonitor() // Check Internet Connection
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied { // Internet Connection is there
                self.schoolListViewModel.getSchoolListFromServer { // Do API Call
                    self.schoolListCollectionView.reloadData() // Update the Collection View with new data
                    self.loadingView?.removeFromSuperview() // Remove Loader screen
                }
            } else { // No Internet Connection
                DispatchQueue.main.async {
                    self.loadingView?.removeFromSuperview() // Remove Loader screen
                    let alert = UIAlertController(title: "Connection Lost!", message: "It seems like there is no internet connection :( ", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
}

// Collection View Setup
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return service.schoolList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: schoolCellIdentifier, for: indexPath) as! SchoolCollectionViewCell
        cell.schoolTitleLabel.text = service.schoolList[indexPath.row].school_name ?? "School Name could not load" // Set School Name
        cell.schoolDBNLabel.text = service.schoolList[indexPath.row].dbn ?? "School DBN Could not load" // Set school DBN
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "SchoolDetailStoryboard", bundle: nil) // Intiate Storyboard
        let controller = storyBoard.instantiateViewController(withIdentifier: schoolDetailStoryboardId) as! SchoolDetailViewController // School Detail Controller
        controller.schoolModel = service.schoolList[indexPath.row] // Send the school Model To the next view model
        self.navigationController?.show(controller, sender: nil) // Navigate to the next View
    }
    
}
