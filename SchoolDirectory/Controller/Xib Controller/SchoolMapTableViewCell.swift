//
//  SchoolMapTableViewCell.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/16/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import UIKit
import MapKit

class SchoolMapTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolMapView: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Set Map Location for a the school address
    func setMapLocation(schoolName: String, latitude: String?, longitude: String?) {
        guard let latitude = latitude else { // Latitude is missing
            return
        }
        guard let longitude = longitude else { // Longitude is missing
            return
        }
        guard let latitudeValue = Double(latitude) else { // Converting Latitude to Double Problem
            return
        }
        guard let longitudeValue = Double(longitude) else { // Converting Longitude to Double problem
            return
        }
        
        let center = CLLocationCoordinate2D(latitude: latitudeValue, longitude: longitudeValue) // School Address on Map
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        schoolMapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.title = schoolName
        annotation.coordinate = center
        schoolMapView.addAnnotation(annotation)
    }
    
}
