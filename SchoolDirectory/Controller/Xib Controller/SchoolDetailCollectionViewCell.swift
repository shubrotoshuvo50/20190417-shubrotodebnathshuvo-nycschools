//
//  SchoolDetailCollectionViewCell.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/17/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import UIKit

class SchoolDetailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = SCHOOLDETAIL_INFO_CELL_COLOR_2
        self.layer.cornerRadius = 15.0
    }

}
