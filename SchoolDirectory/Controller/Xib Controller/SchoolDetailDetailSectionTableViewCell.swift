//
//  SchoolDetailDetailSectionTableViewCell.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/17/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import UIKit

class SchoolDetailDetailSectionTableViewCell: UITableViewCell {

    @IBOutlet weak var detailCollectionView: UICollectionView!
    
    fileprivate let schoolDetailCollectionCell: String = "SchoolDetailCollectionCellId" // Cell Identifier for School Detail
    
    private let schoolListViewModel = SchoolListViewModel() // The View Model
    private var cellList: [(cellName: String, cellInfo: String)] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        detailCollectionView.dataSource = self
        detailCollectionView.delegate = self
        
        detailCollectionView.register(UINib(nibName: "SchoolDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: schoolDetailCollectionCell) // Register Cell For Use
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        setupCellSize() // Setup Cell Size for collection view
    }
    
    // Get data from the server
    func callServer(schoolDbn: String) {
        schoolListViewModel.getSATScoreFromAPI(dbn: schoolDbn) { (satSchoolModel) in
            self.cellList = [
                (cellName: "DBN", cellInfo: satSchoolModel.dbn ?? "" ),
                (cellName: "Math", cellInfo: satSchoolModel.sat_math_avg_score ?? "" ),
                (cellName: "Writing", cellInfo: satSchoolModel.sat_writing_avg_score ?? "" ),
                (cellName: "Reading", cellInfo: satSchoolModel.sat_critical_reading_avg_score ?? "" ),
            ]
            self.detailCollectionView.reloadData()
        }
    }
    
}

// All private function will go here
extension SchoolDetailDetailSectionTableViewCell {
    // Setup the size of the cell
    private func setupCellSize() {
        
        let itemInOneLine: CGFloat = 2 // Total Item in one line
        let itemSpacing: CGFloat = 10 // Space in the sides of the cell
        let itemLineSpacing: CGFloat = 15 // Space between the cells
        let sideInset: CGFloat = 10 // Space beside the side of the collection View
        
        let rowWidth = contentView.frame.width - itemSpacing * CGFloat(itemInOneLine - 1) - (sideInset * 2) // Determine the width of the row eliminating all the spaces
        let cellWidth = rowWidth/itemInOneLine // Determine the Width of a particular cell
        
        let layout = detailCollectionView.collectionViewLayout as! UICollectionViewFlowLayout // Setup Layout
        layout.sectionInset = UIEdgeInsets(top: 20, left: sideInset, bottom: 20, right: sideInset) // Set the inset
        layout.itemSize = CGSize(width: floor(cellWidth), height: 200) // Set the size of the cell
        layout.minimumLineSpacing = itemLineSpacing // Set the line spacing
        layout.minimumInteritemSpacing = itemSpacing // Set the spacing between items
    }
}

// Collection View Setup
extension SchoolDetailDetailSectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: schoolDetailCollectionCell, for: indexPath) as! SchoolDetailCollectionViewCell
        cell.cellInfo.text = cellList[indexPath.row].cellInfo
        cell.cellTitle.text = cellList[indexPath.row].cellName
        return cell
    }
    
}
