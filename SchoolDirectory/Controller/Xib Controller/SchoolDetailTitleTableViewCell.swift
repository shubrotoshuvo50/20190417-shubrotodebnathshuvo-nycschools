//
//  SchoolDetailTitleTableViewCell.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/17/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import UIKit

class SchoolDetailTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleView.addDefaultBoxPropertyNormal() // Add Default box shadow and corner radius
        titleView.backgroundColor = SCHOOLDETAIL_INFO_CELL_COLOR_1 // Add Color for the title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
