//
//  SchoolCollectionViewCell.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/16/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import UIKit

class SchoolCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var schoolTitleLabel: UILabel!
    @IBOutlet weak var schoolDBNLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = SCHOOLLIST_CELL_COLOR // Cell color for the school list
    }
    
    override func layoutSubviews() {
        self.addDefaultBoxProperty() // Add Shadow and round corner property
    }

}
