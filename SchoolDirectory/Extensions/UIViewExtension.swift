//
//  UIViewExtension.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/16/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    // Add Shadow effect and round corner only for Collection View Cells
    func addDefaultBoxProperty() {
        self.layer.cornerRadius = 15.0 // Add Corner Radius
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
    }
    
    // Add Shadow effect and round corner for normal View
    func addDefaultBoxPropertyNormal() {
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize.init(width: 2, height: 2)
        self.layer.shadowRadius = 5
    }
}
