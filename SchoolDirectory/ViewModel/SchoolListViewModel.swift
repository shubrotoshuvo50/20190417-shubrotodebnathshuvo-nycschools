//
//  SchoolListViewModel.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/17/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import Foundation

class SchoolListViewModel {
    private let schoolListAPIURL: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$$app_token=\(service.apiKey)" // URL with API key
    private let schoolSATAPIURL: String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?$$app_token=\(service.apiKey)" // URL with API key for SAT Information
    
    init() {
        
    }
    
    public func getSchoolListFromServer(completionHandler: @escaping () -> Void ) {
        guard let url = URL(string: self.schoolListAPIURL) else { return } // Create URL
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            // Show Loader Screen
            guard error == nil else { // Check error for HTTP Response
                print("Error \(error)")
                return
            }
            
            guard let data = data else { // No Data from http response
                return
            }
            
            guard let schoolList = try? JSONDecoder().decode([SchoolModel].self, from: data) else { // Got all the necessary Data
                print("Could not Decode in the school Model")
                return
            }
            service.schoolList = schoolList // Set it to the School List Array in service
            DispatchQueue.main.async {
                completionHandler() // Function to run after everything is completed
            }
        }
        dataTask.resume()
    }
    
    // Get SAT Score from API
    public func getSATScoreFromAPI(dbn: String?, completionHandler: @escaping (_ satSchoolData: SATModel) -> Void ) {
        guard let dbn = dbn else { return }
        let defaultSATModel = SATModel.init(dbn: dbn, sat_critical_reading_avg_score: "---", sat_math_avg_score: "---", sat_writing_avg_score: "---") // If no Data is found
        let urlString = self.schoolSATAPIURL + "&dbn=" + dbn // SAT Score of a Particular Score
        guard let url = URL(string: urlString) else { return } // Create URL
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            // Show Loader Screen
            guard error == nil else { // Check error for HTTP Response
                return
            }
            
            guard let data = data else { // No Data from http response
                return
            }
            
            guard let SATSchoolData = try? JSONDecoder().decode([SATModel].self, from: data) else { // Got all the necessary Data
                print("Could not Decode in the school Model")
                return
            }
            DispatchQueue.main.async {
                if SATSchoolData.count >= 1 { // Check if the data is peresent
                    completionHandler(SATSchoolData[0]) // Function to run after everything is completed
                } else {
                    completionHandler(defaultSATModel) // If no data found then give default model
                }
            }
        }
        dataTask.resume()
    }
}
