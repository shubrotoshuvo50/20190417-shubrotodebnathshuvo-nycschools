//
//  SchoolModel.swift
//  SchoolDirectory
//
//  Created by IMCS2 on 4/17/19.
//  Copyright © 2019 Shubroto. All rights reserved.
//

import Foundation

struct SchoolModel: Codable {
    var school_name: String?
    var dbn: String?
    var latitude: String?
    var longitude: String?
}
